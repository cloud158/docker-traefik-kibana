
Docker Compose application for deploying [Traefik](https://traefik.io/), [Elasticsearch](https://www.elastic.co/), [Kibana](https://www.elastic.co/kibana/), [Filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-overview.html), [Dozzle](https://dozzle.dev/) in Docker containers.

The individual components are:
- **Traefik**: edge router/reverse proxy which will auto-generate and auto-renew TLS certificates using [Let's Encrypt](https://letsencrypt.org/). This means that all data sent to and from Grafana and InfluxDB will be encrypted.
- **Elasticsearch**: a distributed, free and open search and analytics engine for all types of data, including textual, numerical, geospatial, structured, and unstructured.
- **Kibana**: a free and open user interface that lets you visualize your Elasticsearch data and navigate the Elastic Stack.
- **Filebeat**: a lightweight shipper for forwarding and centralizing log data..
- **Dozzle**: a simple and responsive application that provides you with a web based interface to monitor your Docker container logs live.

## Prerequisites

- [Docker](https://docs.docker.com/get-docker/)

## How to run locally

Deploy the `docker-compose` application:

```bash
docker-compose up
```
You can then access Kibana at [kibana.docker.localhost](http://kibana.docker.localhost). Use the credentials in [.env](.env) to log in to Kibana. 
Kibana is accessible from the HTTP ports (`5061`).

You can then access Dozzle from the HTTP ports (`8081`).

You can then access Traefik from the HTTP ports (`8080`).

## General info

- Kibana will automatically be set up with Elasticsearch as a data source

- Most settings that should be tweaked are provided in [`.env`](./.env) and in  [`config`](./config) folder.

- Certificates can be stored in [`certs`](./certs) folder and configued in _Traefik dynamic configuration_ file [`config/config.yml`](./config/config.yml)
